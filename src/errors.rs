use std::{error::Error, fmt::Display};

pub type TResult<T> = Result<T, Failure>;

#[derive(Debug)]
pub enum Failure {
    FileContentCopy,
    FileUnlink,
    FileMemoryMap,
    ExeReboot,
    FileNameFetch,
    FileHandleAcquire,
    FileContentParse,
}

impl Display for Failure {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{} failed!",
            match self {
                Self::FileContentCopy => "Copying content to the given path",
                Self::FileUnlink => "Unlinking the current executable",
                Self::FileMemoryMap => "Mapping the memory space of the current program",
                Self::ExeReboot => "Restarting the current program",
                Self::FileNameFetch => "Getting the path to the running executable",
                Self::FileHandleAcquire => "Acquiring a handle to the given path",
                Self::FileContentParse => "Parsing the content of the file at the given path",
            }
        )
    }
}

impl Error for Failure {}
