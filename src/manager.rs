use crate::prelude::*;
use memmap2::{MmapMut, MmapOptions};
use object::{File, Object, ObjectSection};
use self_replace;
use std::{
    env,
    fmt::Display,
    fs::{self, OpenOptions},
    path::{Path, PathBuf},
    process::{self, Command},
};

pub struct AssetMgr {
    current_exe: PathBuf,
    copy: PathBuf,
}

// FIXME: possibly broken on Win32
impl AssetMgr {
    pub fn builder() -> AssetMgrBuilder {
        AssetMgrBuilder::default()
    }
    fn _try_get_section<S: Display>(&self, n: S) -> TResult<((u64, u64), MmapMut)> {
        let file = OpenOptions::new()
            .read(true)
            .write(true)
            .open(self.copy.clone())
            .map_err(|_| Failure::FileHandleAcquire)?;
        let buf =
            unsafe { MmapOptions::new().map_mut(&file) }.map_err(|_| Failure::FileMemoryMap)?;
        let file = File::parse(&*buf).map_err(|_| Failure::FileContentParse)?;
        for sec in file.sections() {
            if let Ok(name) = sec.name() {
                if name == n.to_string() {
                    let (min, max) = sec.file_range().ok_or(Failure::FileContentParse)?;
                    return Ok(((min, max), buf));
                }
            }
        }
        Err(Failure::FileContentParse)
    }
    pub fn explicit_cleanup(&self, replace_running: bool) -> TResult<()> {
        if replace_running {
            self_replace::self_replace(self.copy.clone()).map_err(|_| Failure::FileContentCopy)?;
        }
        fs::remove_file(self.copy.clone()).map_err(|_| Failure::FileUnlink)?;
        Ok(())
    }
    pub fn vanish(&self) -> TResult<()> {
        self.explicit_cleanup(false)?;
        houdini::disappear().map_err(|_| Failure::FileUnlink)?;
        process::exit(0);
        #[allow(unreachable_code)]
        Ok(())
    }
    pub fn reboot_self(&self) -> TResult<()> {
        self.explicit_cleanup(true)?;
        Command::new(self.current_exe.display().to_string())
            .spawn()
            .map_err(|_| Failure::ExeReboot)?;
        process::exit(0);
        #[allow(unreachable_code)]
        Ok(())
    }
    pub fn is_initialized(&self, raw_asset_bytes: &[u8]) -> bool {
        let asset_len = raw_asset_bytes.len();
        let asset_zeroes_len = raw_asset_bytes.iter().filter(|byte| *byte == &0u8).count();

        asset_len != asset_zeroes_len
    }
    pub fn set_value_of<S: Display>(&mut self, asset_n: S, new_bytes: &[u8]) -> TResult<()> {
        let ((base, end), mut buf) = self._try_get_section(asset_n)?;
        buf[base as usize..(base as usize + end as usize)].copy_from_slice(new_bytes);
        Ok(())
    }
}

impl Drop for AssetMgr {
    fn drop(&mut self) {
        self.explicit_cleanup(true).unwrap();
    }
}

#[derive(Default)]
pub struct AssetMgrBuilder {
    current_exe: Option<PathBuf>,
    copy: Option<PathBuf>,
}

impl AssetMgrBuilder {
    pub fn current_exe(mut self, exe: PathBuf) -> Self {
        self.current_exe = Some(exe);
        self
    }
    pub fn copy_with_extension<S: Display>(mut self, e: S) -> Self {
        let exe = self.current_exe.clone().unwrap();
        let path_to_copy = exe.with_extension(e.to_string());
        fs::copy(&exe, &path_to_copy)
            .map_err(|_| Failure::FileContentCopy)
            .unwrap();
        self.copy = Some(Path::new(&path_to_copy).into());
        self
    }
    pub fn try_build(self) -> TResult<AssetMgr> {
        let current_exe = self
            .current_exe
            .unwrap_or(env::current_exe().map_err(|_| Failure::FileNameFetch)?);
        // lazy
        let copy = self.copy.ok_or(Failure::FileNameFetch)?;
        Ok(AssetMgr { current_exe, copy })
    }
}
